import { Component, OnInit, HostBinding } from '@angular/core';
import { GamesService } from '../../services/games.service'
  import { from } from 'rxjs';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  games:any = [];

  constructor(private gameService: GamesService) { }

  ngOnInit() {
    this.getGame();
  }
  getGame(){
    this.gameService.getGames().subscribe(
      res => {
        this.games = res;
      } ,
      err => console.log(err)
    )
  }

  deleteGame(id: string){
    this.gameService.deleteGame(id).subscribe(
      res => {
        console.log(res)
        this.getGame();
      },
      err => console.log(err)
    )
  }

}
